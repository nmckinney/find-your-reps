//
//  RepTitleBar.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RepTitleBar.h"

@implementation RepTitleBar
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0.549 green:0.776 blue:0.247 alpha:1];
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        titleLabel.text = @"Find Your Reps";
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont fontWithName:@"KievitOT-Bold" size:30];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.textAlignment = UITextAlignmentCenter;
        [self addSubview:titleLabel];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextMoveToPoint(context, 0, self.frame.size.height - 1);
    CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height - 1);
    CGContextStrokePath(context);
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextMoveToPoint(context, 0, self.frame.size.height);
    CGContextAddLineToPoint(context, self.frame.size.width, self.frame.size.height);
    CGContextStrokePath(context);
}

-(void) addBackButton
{
    UIImage *arrowImg = [UIImage imageNamed:@"arrow_back.png"];
    backButton = [[UIImageView alloc] initWithFrame:CGRectMake(12, 14, 16, 16)];
    backButton.image = arrowImg;
    backButton.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self.delegate action:@selector(backButtonWasPressed)];
    [backButton addGestureRecognizer:tapped];
    [self addSubview:backButton];
}



@end
