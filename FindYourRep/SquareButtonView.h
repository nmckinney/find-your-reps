//
//  SquareButtonView.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SquareButtonDelegate <NSObject>

-(void) squareButtonWasPressed: (UIButton*) button;

@end

@interface SquareButtonView : UIButton

@property (readonly) UILabel *label;
@property (readonly) UIImageView *image;
@property (readonly) bool isActive;
@property (assign, nonatomic) id <SquareButtonDelegate> delegate;
@property short int index;
-(void) toggleButton;
- (id)initWithFrame:(CGRect)frame andDelegate: (id <SquareButtonDelegate>) delegate andIndex: (int) index active: (bool) active andImage: (UIImage*) img andLabelText:(NSString*) text;
@end
