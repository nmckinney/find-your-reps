//
//  Constants.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define SUNLIGHT_BASE @"http://services.sunlightlabs.com/api/"
#define GOVTRACK_BASE @"http://www.govtrack.us/api/v1/"
#define SUNLIGHT_API_KEY @"755527c41f6944f586ee7fe75e52dde6"

#define SUNLIGHT_BY_ZIP @"legislators.allForZip.json?zip="
#define SUNLIGHT_BY_STATE @"legislators.getList.json?state="

#define GOVTRACK_PHOTO @"http://www.govtrack.us/data/photos/"
#define PX100 @"-100px.jpeg"
#define kURLConnectionCompleted @"NSURLConnectionCompleted"