//
//  NMPickerInput.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NMPickerInput : UIView 

@property (readonly) UIPickerView *picker;

- (id)initWithFrame:(CGRect)frame andPickerDelegate: (id <UIPickerViewDelegate>) delegate andPickerDataSource: (id <UIPickerViewDataSource>) dataSource;

@end
