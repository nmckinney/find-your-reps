//
//  RepresentativeSelection.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RepresentativeSelection.h"

@interface RepresentativeSelection ()

@end

@implementation RepresentativeSelection

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [legislatorArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *cellid = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }   
    NSString *firstName = [[legislatorArray objectAtIndex:indexPath.row] objectForKey:@"firstname"];
    NSString *lastName = [[legislatorArray objectAtIndex:indexPath.row] objectForKey:@"lastname"];
    NSString *party = [[legislatorArray objectAtIndex:indexPath.row] objectForKey:@"party"];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    cell.imageView.image = 
    ([party isEqualToString:@"D"]) ? [UIImage imageNamed:@"donkey_political.png"] :
    ([party isEqualToString:@"R"]) ? [UIImage imageNamed:@"elephant_political.png"] : nil;     
    
    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    ds = [DataSingleton sharedDataSingleton];
    legislatorArray = [[NSMutableArray alloc] init];
    republicanArray = [[NSMutableArray alloc] init];
    democratArray = [[NSMutableArray alloc] init];
    independentArray = [[NSMutableArray alloc] init];
    
    for (id obj in [ds legislatorArray]) {
        if ([[[(NSDictionary*)obj objectForKey:@"legislator"] objectForKey:@"chamber"] isEqualToString:(ds.visible == SENATOR_VIS) ? @"senate" : @"house"]) {
            [legislatorArray addObject:[(NSDictionary*)obj objectForKey:@"legislator"]];
            
            NSDictionary *legislatorDict = [(NSDictionary*)obj objectForKey:@"legislator"];
            NSString *party = [legislatorDict objectForKey:@"party"];
    
            if ([party isEqualToString:@"D"]) {
                [democratArray addObject:legislatorDict];
            }
            else if ([party isEqualToString:@"R"]) {
                [republicanArray addObject:legislatorDict];
            }
            else {
                [independentArray addObject:legislatorDict];
            }
            
        }
    }
    
    [legislatorArray sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"lastname" ascending:YES]]];
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(20, 80, self.view.frame.size.width - 40, self.view.frame.size.height - 80) style:UITableViewStylePlain];
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
        
    table.backgroundColor = [UIColor clearColor];
    table.layer.cornerRadius = 10;    
    self.view.backgroundColor = [UIColor colorWithRed:0.384 green:0.792 blue:0.89 alpha:1];
    
    headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 50, self.view.frame.size.width - 100, 30)];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.textAlignment = UITextAlignmentCenter;
    headerLabel.backgroundColor = [UIColor colorWithRed:0.02 green:0.424 blue:0.714 alpha:1];
    headerLabel.font = [UIFont fontWithName:@"KievitOT" size:30];
    headerLabel.text = (ds.visible == SENATOR_VIS) ? @"Senators" : @"Representatives";
    
    headerLabel.layer.shadowRadius = 3;
    headerLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    headerLabel.layer.shadowOpacity = 0.6;
    headerLabel.layer.masksToBounds = NO;
    [self.view addSubview:headerLabel];
    
    titleBar = [[RepTitleBar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    titleBar.delegate = self;
    [titleBar addBackButton];
    [self.view addSubview:titleBar];
}

-(void) backButtonWasPressed 
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
