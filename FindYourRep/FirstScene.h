//
//  ViewController.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define kHouse 1
#define kSenate 2
#define kZip 3
#define kState 4

#import <UIKit/UIKit.h>
#import "FindRepButton.h"
#import "CustomTextField.h"
#import "SquareButtonView.h"
#import "RepTitleBar.h"
#import "NMPickerInput.h"
#import "AFJSONRequestOperation.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "DataSingleton.h"

@interface FirstScene : UIViewController 
    <UITextFieldDelegate, FindRepButtonDelegate, SquareButtonDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    CustomTextField *textField;
    FindRepButton *searchButton;
    SquareButtonView *houseButton;
    SquareButtonView *senateButton;
    SquareButtonView *zipButton;
    SquareButtonView *stateButton;
    RepTitleBar * titleBar;
    NSDictionary *dictForStates;
    NSArray *buttons;
    NSMutableArray *buttonsRevert;
    UIView *containerView;
    CGRect originalFrame;
    CGRect scrolledFrame;
    NSArray *sortedStateKeys;
    NSArray *sortedStateAbbr;
    CGRect originalTextFieldFrame;
    CGRect scrolledTextFieldFrame;
    NMPickerInput *pickerInput;
    AFJSONRequestOperation *jsonRequest;
}

@end
