//
//  NetworkSingleton.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DataSingleton.h"

@implementation DataSingleton
@synthesize legislatorArray;
@synthesize visible;

static DataSingleton *sharedInstance;

-(id) init 
{
    if (self = [super init]) {
        legislatorArray = [[NSArray alloc] init];
        visible = HOUSE_VIS;
    }
    return self;
}

+(id) sharedDataSingleton 
{
    if (!sharedInstance) {
        sharedInstance = [[DataSingleton alloc] init];
    }
    return sharedInstance;
}

@end
