//
//  SquareButtonView.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SquareButtonView.h"
#import <QuartzCore/QuartzCore.h>

@implementation SquareButtonView
@synthesize index;
@synthesize delegate;
@synthesize isActive;
@synthesize image;
@synthesize label;

- (id)initWithFrame:(CGRect)frame andDelegate:(id<SquareButtonDelegate>)del andIndex:(int)ind active: (bool) active andImage: (UIImage*) img andLabelText: (NSString*)text
{
    self = [super initWithFrame:frame];
    if (self) {
        if (del) {
            self.delegate = del;
            [self addTarget:self.delegate action:@selector(squareButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (ind) {
            index = ind;
        }
        
        image = [[UIImageView alloc] initWithFrame:CGRectMake(13, 15, img.size.width, img.size.height)];
        image.image = img;
        image.backgroundColor = [UIColor clearColor];
        [self addSubview:image];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 20, self.frame.size.width, 20)];
        label.font = [UIFont fontWithName:@"KievitOT" size:16];
        label.text = text;
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = UITextAlignmentCenter;
        [self addSubview:label];
        
//        self.backgroundColor = [UIColor colorWithRed:0.98 green:0.651 blue:0.204 alpha:1]; //selected color
        self.backgroundColor = [UIColor colorWithRed:0.761 green:0.804 blue:0.137 alpha:1];
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 1;
        
        self.layer.shadowRadius = 4;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.6;
        
        isActive = (active) ? YES : NO;
    }
    return self;
}

-(void) toggleButton {
    if (isActive) {
        isActive = NO;
//        self.backgroundColor = [UIColor colorWithRed:0.761 green:0.804 blue:0.137 alpha:1];
        [UIView animateWithDuration:0.5 animations:^{
            self.layer.opacity = 0.3; 
        }];
    }
    else {
        isActive = YES;
        [UIView animateWithDuration:0.5 animations:^{
            self.layer.opacity = 1;  
        }];
//        self.backgroundColor = [UIColor colorWithRed:0.98 green:0.651 blue:0.204 alpha:1];
    }
}

@end
