//
//  NMPickerInput.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NMPickerInput.h"
#import <QuartzCore/QuartzCore.h>

@implementation NMPickerInput
@synthesize picker;

- (id)initWithFrame:(CGRect)frame andPickerDelegate: (id <UIPickerViewDelegate>) delegate andPickerDataSource: (id <UIPickerViewDataSource>) dataSource
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor colorWithRed:0.384 green:0.792 blue:0.89 alpha:1];
        self.backgroundColor = [UIColor greenColor];
        
        
        self.layer.shadowRadius = 4;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.6;
        
        self.layer.cornerRadius = 5;
        
        picker = [[UIPickerView alloc] init];
        picker.dataSource = dataSource;
        picker.delegate = delegate;
        picker.layer.cornerRadius = 5;
        picker.showsSelectionIndicator = YES;
        [self addSubview:picker];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
