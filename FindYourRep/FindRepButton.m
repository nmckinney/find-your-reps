//
//  FindRepButton.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FindRepButton.h"
#import <QuartzCore/QuartzCore.h>

@implementation FindRepButton
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, 500, frame.size.width, frame.size.height)];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0.02 green:0.424 blue:0.714 alpha:1];
        [UIView animateWithDuration:2 animations:^{
            self.frame = frame; 
        }];
        
        findLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        findLabel.text = @"SEARCH";
        findLabel.font = [UIFont fontWithName:@"KievitOT-Bold" size:28];
        findLabel.backgroundColor = [UIColor clearColor];
        findLabel.textAlignment = UITextAlignmentCenter;
        findLabel.textColor = [UIColor whiteColor];
        [self addSubview:findLabel];
        self.showsTouchWhenHighlighted = YES;
        
        self.layer.shadowOpacity = 0.7;
        self.layer.shadowRadius = 2;
        self.layer.masksToBounds = NO;
        
        [self addTarget:self.delegate action:@selector(buttonWasPressed) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}




@end
