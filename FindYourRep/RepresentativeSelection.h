//
//  RepresentativeSelection.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"
#import "RepTitleBar.h"
#import <QuartzCore/QuartzCore.h>

@interface RepresentativeSelection : UIViewController <UITableViewDelegate, UITableViewDataSource, RepTitleBarDelegate>
{
    DataSingleton *ds;
    RepTitleBar *titleBar;
    UILabel *headerLabel;
    NSMutableArray *legislatorArray;
    NSMutableArray *republicanArray;
    NSMutableArray *democratArray;
    NSMutableArray *independentArray;
    UITableView *table;
}

@end
