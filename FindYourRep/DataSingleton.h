//
//  NetworkSingleton.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SENATOR_VIS = 1,
    HOUSE_VIS
}VISIBLE_CONGRESSMEN;

@interface DataSingleton : NSObject
@property (strong, nonatomic) NSArray *legislatorArray;
@property VISIBLE_CONGRESSMEN visible;

+(id) sharedDataSingleton;

@end
