//
//  CustomTextField.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField {
    UILabel *placeholderLabel;
    bool statePickerActive;
    UIPickerView *picker;
}
- (id)initWithFrame:(CGRect)frame andPlaceHolder: (NSString*) placeholder;
@end
