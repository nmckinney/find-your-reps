//
//  ViewController.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FirstScene.h"

@implementation FirstScene

-(void) dataRetrieved 
{
    NSDictionary *jsonDict;
    if ((jsonDict = [jsonRequest responseJSON])) {
        if ([[jsonDict objectForKey:@"response"] objectForKey:@"legislators"]) {
            [[DataSingleton sharedDataSingleton] setLegislatorArray:
             [[jsonDict objectForKey:@"response"] objectForKey:@"legislators"]];
            if (senateButton.isActive) {
                NSLog(@"Senate button is active");
                [[DataSingleton sharedDataSingleton] setVisible:SENATOR_VIS];
            }
            else {
                [[DataSingleton sharedDataSingleton] setVisible:HOUSE_VIS];
            }
            [self performSegueWithIdentifier:@"toTable" sender:self];
        }
    }
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 50;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    textField.text = [sortedStateKeys objectAtIndex:row];
    [textField resignFirstResponder];
}

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component 
{
    return [sortedStateKeys objectAtIndex:row];

}

-(CGFloat) pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return pickerView.frame.size.width * 0.75;
    }
    return pickerView.frame.size.width * 0.25;
}

-(CGFloat) pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 20.0;
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (textField.isEditing) {
        [self textFieldShouldReturn:textField];
    }
}

-(void) textFieldDidBeginEditing:(UITextField *)textField {
    
    UILabel *tempLabel = [textField.leftView.subviews objectAtIndex:0];
    if ([tempLabel.text isEqualToString:@"state"]) {
        textField.inputView = pickerInput;
    }
    else {
        textField.inputView = nil;
    }
    
    [UIView animateWithDuration:1 animations:^{
        for (UIButton *button in buttons) {
            [buttonsRevert addObject:[NSNumber numberWithFloat:button.layer.opacity]];
            button.layer.opacity = 0;
        }
        containerView.frame = scrolledFrame; 
    }];
}

-(void) textFieldDidEndEditing:(UITextField *)textField {
    [UIView animateWithDuration:1 animations:^{
        for (int i = 0; i < [buttons count]; i++) {
            SquareButtonView *button = [buttons objectAtIndex:i];
            button.layer.opacity = [[buttonsRevert objectAtIndex:i] floatValue];
        }
        [buttonsRevert removeAllObjects];
        containerView.frame = originalFrame;
    }];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void) buttonWasPressed {
    NSLog(@"Searching...");
    if (textField.isEditing) {
        [self textFieldShouldReturn:textField];
    }
    if ([textField.text isEqualToString:@""] || !textField.text) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You did not enter any search terms." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([textField.placeholder isEqualToString:@"zip"] && ([textField.text length] < 5)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Zip code not long enough" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSURLRequest *request = 
    [[NSURLRequest alloc] initWithURL:
        [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@%@%@", 
                                        SUNLIGHT_BASE, 
                              (zipButton.isActive) ?  SUNLIGHT_BY_ZIP : SUNLIGHT_BY_STATE, 
                              (zipButton.isActive) ? textField.text : [dictForStates objectForKey:textField.text],
                                        @"&apikey=", 
                                        SUNLIGHT_API_KEY]] 
        cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0f];
    NSLog(@"URL used: %@", [request URL]);
    jsonRequest = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) 
        {
            NSLog(@"Connection succeeded!");
            NSDictionary *tempDict;
            if ((tempDict = [[(NSDictionary*)JSON objectForKey:@"response"] objectForKey:@"legislators"])) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kURLConnectionCompleted object:nil];
            }
        } 
      failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error retrieving data." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
          [alert show];
          NSLog(@"Failed");
      }];
    [jsonRequest start];
}

-(void) swapTextField: (NSString*) newText {
    textField.text = @"";
    [UIView animateWithDuration:1 animations:^{
        textField.frame = scrolledTextFieldFrame; 
    }];
    UILabel *tempLabel = (UILabel*)[textField.leftView.subviews objectAtIndex:0];
    tempLabel.text = newText;
    textField.frame = CGRectMake(textField.frame.size.width * (-1) - 30, textField.frame.origin.y, textField.frame.size.width, textField.frame.size.height);
    [UIView animateWithDuration:1 animations:^{
        textField.frame = originalTextFieldFrame; 
    }];
}


-(void) squareButtonWasPressed: (UIButton*) button {
    if ((SquareButtonView*)button) {
        if ([(SquareButtonView*)button isActive]) 
            return;
        [(SquareButtonView*)button toggleButton];
        switch ([(SquareButtonView*)button index]) {
            case kHouse:
                [senateButton toggleButton];
                break;
            case kSenate:
                [houseButton toggleButton];
                break;
            case kZip:
                [self swapTextField:@"zip"];
                [stateButton toggleButton];
                break;
            case kState:
                [self swapTextField:@"state"];
                [zipButton toggleButton];
                break;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    buttonsRevert = [[NSMutableArray alloc] init];
    
    originalFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    scrolledFrame = CGRectMake(0, -(self.view.frame.size.height) + 240, self.view.frame.size.width, self.view.frame.size.height); 
    originalTextFieldFrame = CGRectMake(30, 325, self.view.frame.size.width - 60, 40);
    scrolledTextFieldFrame = CGRectMake(self.view.frame.size.width + 30, 325, self.view.frame.size.width - 60, 40);
    
    containerView = [[UIView alloc] initWithFrame:originalFrame];
    
	self.view.backgroundColor = [UIColor colorWithRed:0.384 green:0.792 blue:0.89 alpha:1];
    searchButton = [[FindRepButton alloc] initWithFrame:CGRectMake(30, 375, self.view.frame.size.width - 60, 40)];
    searchButton.delegate = self;
    [containerView addSubview:searchButton];

    textField = [[CustomTextField alloc] initWithFrame:originalTextFieldFrame andPlaceHolder:@"state"];
    textField.delegate = self;
    textField.clearsOnBeginEditing = YES;
    textField.keyboardType = UIKeyboardTypeNumberPad;
    [containerView addSubview:textField];
    
    houseButton = [[SquareButtonView alloc] initWithFrame:CGRectMake(30, 60, 125, 125) andDelegate:self andIndex:1 active:YES andImage:[UIImage imageNamed:@"house_icon.png"] andLabelText:@"House"];
    houseButton.backgroundColor = [UIColor colorWithRed:0.98 green:0.651 blue:0.204 alpha:1];
    [containerView addSubview:houseButton];
    
    senateButton = [[SquareButtonView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 0.5 + 5, 60, 125, 125) andDelegate:self andIndex:2 active:NO andImage:[UIImage imageNamed:@"senate_icon.png"] andLabelText:@"Senate"];
    senateButton.layer.opacity = 0.3;
    senateButton.backgroundColor = [UIColor colorWithRed:0.98 green:0.651 blue:0.204 alpha:1];
    [containerView addSubview:senateButton];
    
    zipButton = [[SquareButtonView alloc] initWithFrame:CGRectMake(30, 190, 125, 125) andDelegate:self andIndex:3 active:NO andImage:[UIImage imageNamed:@"zip_icon.png"] andLabelText:@"Zipcode"];
    zipButton.layer.opacity = 0.3;
    [containerView addSubview:zipButton];
    
    stateButton = [[SquareButtonView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * 0.5 + 5, 190, 125, 125) andDelegate:self andIndex:4 active:YES andImage:[UIImage imageNamed:@"state_icon.png"] andLabelText:@"State"];
    [containerView addSubview:stateButton];
    
    buttons = [[NSArray alloc] initWithObjects:houseButton, senateButton, zipButton, stateButton, nil];
    
    titleBar = [[RepTitleBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    [containerView addSubview:titleBar];
    
    [self.view addSubview:containerView];
    
    dictForStates = [[NSDictionary alloc] initWithContentsOfFile:
                   [[NSBundle mainBundle] pathForResource:@"States" ofType:@"plist"]];
    sortedStateKeys = [self stringDictionarySort:dictForStates sortKeys:YES];
    pickerInput = [[NMPickerInput alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 250) andPickerDelegate:self andPickerDataSource:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataRetrieved) name:kURLConnectionCompleted object:nil];
}

-(NSArray*) stringDictionarySort: (NSDictionary*)dict sortKeys:(bool) sortKeys{
    NSArray *sortedArray;
    if (sortKeys) {
        sortedArray = [dict allKeys];
    }
    else {
        sortedArray = [dict allValues];
    }
    sortedArray = [sortedArray sortedArrayUsingComparator:^NSComparisonResult(id objA, id objB){
        return [(NSString*)objA compare:(NSString*)objB options:NSCaseInsensitiveSearch];
    }];
    return sortedArray;
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
