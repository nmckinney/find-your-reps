//
//  FindRepButton.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FindRepButtonDelegate <NSObject> 

-(void) buttonWasPressed;

@end

@interface FindRepButton : UIButton{
    UILabel *findLabel;
}
@property (assign, nonatomic) id <FindRepButtonDelegate> delegate;
@end

