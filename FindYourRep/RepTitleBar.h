//
//  RepTitleBar.h
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RepTitleBarDelegate <NSObject>

-(void) backButtonWasPressed;

@end
@interface RepTitleBar : UIView {
    UIImageView *backButton;
    UILabel *titleLabel;
}
@property (assign, nonatomic) id <RepTitleBarDelegate> delegate;
-(void) addBackButton;
@end
