//
//  CustomTextField.m
//  FindYourRep
//
//  Created by Nicholas McKinney on 8/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomTextField.h"
#import <QuartzCore/QuartzCore.h>

@implementation CustomTextField

- (id)initWithFrame:(CGRect)frame andPlaceHolder: (NSString*) placeholder
{
    self = [super initWithFrame:CGRectMake(-frame.size.width, frame.origin.y, frame.size.width, frame.size.height)];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.font = [UIFont fontWithName:@"KievitOT" size:20];
        self.layer.borderColor = [UIColor colorWithRed:0.65 green:0.65 blue:0.65 alpha:1].CGColor;
        self.layer.borderWidth = 1;
        
        self.leftView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 45, 40)];
        self.leftViewMode = UITextFieldViewModeAlways;
        
        placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.leftView.frame.size.width, self.leftView.frame.size.height)];
        placeholderLabel.text = placeholder;
        placeholderLabel.textColor = [UIColor colorWithRed:0.675 green:0.682 blue:0.675 alpha:1];
        placeholderLabel.textAlignment = UITextAlignmentCenter;
        [self.leftView addSubview:placeholderLabel];
        
        [UIView animateWithDuration:1 animations:^{
            self.frame = frame; 
        }];
        
        statePickerActive = YES;
    }
    return self;
}

@end
